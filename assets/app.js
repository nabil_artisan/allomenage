
require('bootstrap')

import './styles/app.scss';

// start the Stimulus application
import './bootstrap';

function importAll(r) {
    return r.keys().map(r);
}

const fonts = importAll(require.context('./fonts', true, /\.(ttf|otf?g)$/));

const images = importAll(require.context('./images', true, /\.(png|jpe?g)$/));
